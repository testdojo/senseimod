package senseimod

// RegistrationRequest ...
type RegistrationRequest struct {
	Service   string                     `json:"service"`
	PackageID string                     `json:"package_id"`
	Name      string                     `json:"name"`
	Path      string                     `json:"path"`
	Schedules []RegistrationTestSchedule `json:"schedules"`
}

// RegistrationTestSchedule ...
type RegistrationTestSchedule struct {
	Env                 string `json:"env" mapstructure:"env"`
	URL                 string `json:"url" mapstructure:"url"`
	RecurrenceTimeDelta uint32 `json:"recurrence_time_delta" mapstructure:"recurrence_time_delta"`
}

// RegistrationResult ...
type RegistrationResult struct {
	BaseResult
}
