package senseimod

// CreateAccountRequest ...
type CreateAccountRequest struct {
	Name   string `json:"name"`
	Domain string `json:"domain"`

	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Pwd       string `json:"pwd"`
}

// CreateAccountResult ...
type CreateAccountResult struct {
	BaseResult

	Data *CreateAccountData `json:"data,omitempty"`
}

// CreateAccountData ...
type CreateAccountData struct {
	Account *AccountSummary `json:"account,omitempty"`
	UserID  string          `json:"user_id,omitempty"`
	Token   string          `json:"token,omitempty"`
}

// AccountSummary ...
type AccountSummary struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Domain string `json:"domain"`
}
