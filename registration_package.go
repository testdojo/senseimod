package senseimod

// RegistrationPackageResult ...
type RegistrationPackageResult struct {
	BaseResult

	ID string `json:"id"`
}
